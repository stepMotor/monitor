/*
 * camer.h
 *
 *  Created on: 2014-8-16
 *      Author: liu
 */

#ifndef CAMER_H_
#define CAMER_H_

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <linux/videodev2.h>
#include <unistd.h>

typedef struct frameBuf {
	unsigned char *start;
	int length;
} Frame_buf;

typedef struct camer_s {
	int camera_fd;
	int n_buffer;
	__u32 width;
	__u32 height;
	__u32 type;
	Frame_buf *buf;
} Camera;

void write_data_to_file(unsigned char *ptr, int length, char *filename);
Camera* calloc_camera();
void free_camera(Camera* ptr);
int captureInit(Camera *args);
int read_frame(Camera *args, unsigned char *dest);
void stop_capturing(Camera *args);


#endif /* CAMER_H_ */
