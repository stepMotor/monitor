/*
 * encode.h
 *
 *  Created on: 2014-8-17
 *      Author: liu
 */

#ifndef ENCODE_H_
#define ENCODE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <dlfcn.h>
//encode head file
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
//swscale head file
#include <libavutil/avutil.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>

typedef struct _encode {
	AVCodecContext* pCodecCtx;
	AVBitStreamFilterContext* bsfc;
	AVCodec* pCodec;
	AVFrame* picture;
	struct SwsContext *sws_ctx;
	char* out_file;
	uint8_t* picture_buf;
	uint8_t* src_data[4];
	int src_linesize[4];
	int src_size;
	int in_w;
	int in_h;
	int size;
	AVPacket pkt;
	FILE* tmp_file_fd;
} Encode;

int ffmpeg_init(Encode* encode);
int ffmpeg_encode_end(Encode *encode);

#endif /* ENCODE_H_ */
