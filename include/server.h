/*
 * server.h
 *
 *  Created on: 2014-8-17
 *      Author: liu
 */

#ifndef SERVER_H_
#define SERVER_H_

#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <malloc.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>

#define MAXLINE 20
#define OPEN_MAX 100
#define LISTENQ 20
#define SERV_PORT 5000
#define INFTIM 1000

typedef struct ownSocket {
	char *dist_addr;
	int listenfd, connfd, sockfd, epfd;
	int nfds, portnumber;
	struct sockaddr_in clientaddr;
	struct sockaddr_in serveraddr;
	struct epoll_event ev, events[20];
	socklen_t clilen;
	char car_control[4];
} own_socket;

#endif /* SERVER_H_ */
